FROM openjdk

# Install
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm

RUN apt-get update && \
    apt-get install -y \
      maven \
      vim \
    && \
    rm -r /var/lib/apt/lists/* 
ADD bin/* /usr/local/bin/
ADD data /data

RUN chmod a+rx /usr/local/bin/* && \
    setup.sh && rm /usr/local/bin/setup.sh

WORKDIR /

CMD ["bash"]
