#!/bin/bash

rand() {
  < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-4};echo;
}

for l in $(printf "%s " {a..z}); do
  dir=/find-files/$l
  
  for n in $(seq 1 5); do 
    seq=$(($RANDOM % 3))
    for d in $(seq 1 $seq); do
      dir="${dir}/$(rand)"
    done
    mkdir -p $dir
    if [ $(($RANDOM & 3)) -eq 0 ]; then
      touch $dir/T$(rand)
    else
      touch -t '197001010000' $dir/$(rand)
    fi
  done
done

for l in $(printf "%s " {b..z}); do
  cp /data/a.csv /data/$l.csv
done

cd /
mvn archetype:generate -DgroupId=edu.vt -DartifactId=java-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
cd /java-app
rm -rf src/test
mvn clean package
mvn clean
cat >src/main/java/edu/vt/App.java <<EOF
package edu.vt;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" )
    }
}
EOF
clear
