#!/bin/bash
set -ex
git clone https://code.vt.edu/rquintin/scenario-linux-java.git
cd scenario-linux-java
docker build -t scenario . && docker run --rm -it scenario
